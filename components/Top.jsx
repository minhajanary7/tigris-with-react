function Top (){
    return(
        <header>
        <div className="container">
          <a id="logo" href="#">
            <span>Tigris</span>
          </a>
          <button className="menuicon">
            <span className="material-symbols-outlined">pets</span>
          </button>
          <nav>
            <ul>
              <li>
                <a href="#">Home</a>
              </li>
              <li>
                <a href="#">About</a>
              </li>
              <li>
                <a href="#">Services</a>
              </li>
              <li>
                <a href="#">Contact us</a>
              </li>
            </ul>
          </nav>
        </div>
      </header>
    )
}
export default Top