function Productcard(){
    return(
        <article className="blog">
        <img src="./images/tiger1.avif" alt="" />
        <h3>Majestic Tigers: Capturing the Essence of Wilderness</h3>
        <p>
          Behold the epitome of wilderness captured in the captivating image
          of majestic tigers. Here, amidst the untamed beauty of nature,
          these magnificent creatures reign supreme, embodying strength,
          grace, and untamed beauty.
        </p>
      </article>
    )
}
export default Productcard