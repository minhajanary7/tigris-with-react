import './App.css'
import Productcard from './components/Productcard'
import Top from './components/Top'


function App() {


  return (
    <>
  <>
 <Top/>
  <main>
    <div className="sectionone">
      <section className="container">
        <h1>Haven of the Striped Majesty</h1>
        <p>
          Welcome to Tigris Haven: Preserving Majesty in the Tiger Reservoir.
          Immerse yourself in the extraordinary world of Tigris, where
          conservation meets the wild. Explore our efforts to protect and
          sustain these majestic creatures. Join us in safeguarding their
          habitat and ensuring a harmonious coexis tence between humanity and the
          awe-inspiring Tigris.
        </p>
        <div className="buttons">
          <a className="buttonholder buttonprimary" href="#">
            <span>Book Safari </span>
          </a>
          <a className="buttonholder buttonsecondary" href="#">
            <span>Contact us</span>
          </a>
        </div>
      </section>
    </div>
    <section id="aboutsection">
      <div className="container">
        <h2>Preserving the Legacy of Tigers at Our Reservoir</h2>
        <p>
          Discover the epitome of wildlife conservation at our Tiger Reservoir.
          Nestled amidst pristine landscapes, we're dedicated to preserving and
          protecting the majestic tiger species. Through sustainable practices
          and community engagement, we strive to ensure a harmonious coexistence
          between humans and these awe-inspiring creatures. Join us in
          safeguarding nature's legacy.
        </p>
      </div>
    </section>
    <section>
      <div className="container">
        <h2>Roars and Insights</h2>
        <div className="bloglist">
         <Productcard/>
          <article className="blog">
            <img src="./images/tiger7.avif" alt="" />
            <h3>Roaring Royalty: A Tiger's Dominance</h3>
            <p>
              Behold the epitome of wilderness captured in the captivating image
              of majestic tigers. Here, amidst the untamed beauty of nature,
              these magnificent creatures reign supreme, embodying strength,
              grace, and untamed beauty.
            </p>
          </article>
          <article className="blog">
            <img src="./images/tiger6.avif" alt="" />
            <h3>Wild Ambassadors: Tigers in Their Element</h3>
            <p>
              Behold the epitome of wilderness captured in the captivating image
              of majestic tigers. Here, amidst the untamed beauty of nature,
              these magnificent creatures reign supreme, embodying strength,
              grace, and untamed beauty.
            </p>
          </article>
          <article className="blog">
            <img src="./images/tiger5.avif" alt="" />
            <h3>Fierce Guardians: Tigers Protecting Their Territory</h3>
            <p>
              Behold the epitome of wilderness captured in the captivating image
              of majestic tigers. Here, amidst the untamed beauty of nature,
              these magnificent creatures reign supreme, embodying strength,
              grace, and untamed beauty.
            </p>
          </article>
          <article className="blog">
            <img src="./images/tiger3.webp" alt="" />
            <h3>Serenity in Strength: A Tiger's Zen</h3>
            <p>
              Behold the epitome of wilderness captured in the captivating image
              of majestic tigers. Here, amidst the untamed beauty of nature,
              these magnificent creatures reign supreme, embodying strength,
              grace, and untamed beauty.
            </p>
          </article>
          <article className="blog">
            <img src="./images/tiger2.avif" alt="" />
            <h3>Captivating Gaze: Eyes of the Tiger</h3>
            <p>
              Behold the epitome of wilderness captured in the captivating image
              of majestic tigers. Here, amidst the untamed beauty of nature,
              these magnificent creatures reign supreme, embodying strength,
              grace, and untamed beauty.
            </p>
          </article>
        </div>
      </div>
    </section>
  </main>
  <footer></footer>
</>

    </>
  )
}

export default App
